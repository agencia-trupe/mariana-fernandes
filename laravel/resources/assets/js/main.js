(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.envioNewsletter = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#response-newsletter');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/newsletter',
            data: { email: $('#newsletter_email').val() },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                $response.hide().text(data.responseJSON.email).fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.envioComentario = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#response-comentario');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $form.data('url'),
            data: {
                autor: $('#autor').val(),
                email: $('#comentario_email').val(),
                comentario: $('#comentario').val()
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().html('<p>' + data.message + '</p>').fadeIn('slow');
            },
            error: function(data) {
                var message = '';
                $.each(data.responseJSON, function(key, value) {
                    message += '<p class="error">' + value + '</p>';
                });

                $response.hide().html(message).fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        $('#form-newsletter').on('submit', this.envioNewsletter);
        $('#form-comentario').on('submit', this.envioComentario);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));

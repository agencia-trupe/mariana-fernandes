@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('blog_categoria_id', 'Categoria') !!}
    {!! Form::select('blog_categoria_id', $categorias, '', ['class' => 'form-control select-categorias', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('blog_post_id', 'Post') !!}
    {!! Form::select('blog_post_id', [], null, ['class' => 'form-control', 'placeholder' => 'Selecione uma Categoria']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.blog.destaques.index') }}" class="btn btn-default btn-voltar">Voltar</a>

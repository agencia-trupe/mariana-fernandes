@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.blog.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Blog
    </a>

    <legend>
        <h2>
            <small>Blog /</small> Destaques
            @if(count($destaques) < $limite)
            <a href="{{ route('painel.blog.destaques.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Destaque</a>
            @endif
        </h2>
    </legend>

    @if(!count($destaques))
    <div class="alert alert-warning" role="alert">Nenhum destaque cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($destaques as $destaque)
            <tr class="tr-row">
                <td>{{ $destaque->data }}</td>
                <td>{{ $destaque->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.blog.destaques.destroy', $destaque->id), 'method' => 'delete')) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection

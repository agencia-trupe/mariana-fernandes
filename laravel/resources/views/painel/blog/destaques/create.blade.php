@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Blog /</small> Adicionar Destaque</h2>
    </legend>

    {!! Form::open(['route' => 'painel.blog.destaques.store']) !!}

        @include('painel.blog.destaques.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop

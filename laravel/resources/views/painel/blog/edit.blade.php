@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Blog /</small> Editar Post</h2>
    </legend>

    {!! Form::model($post, [
        'route' => ['painel.blog.update', $post->id],
        'method' => 'patch',
        'files' => true])
    !!}

        @include('painel.blog.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop

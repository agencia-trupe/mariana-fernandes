<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url() }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ config('site.description') }}">
    <meta name="keywords" content="{{ config('site.keywords') }}">

@if(Route::currentRouteName() == 'artigos.post' && isset($post))
    <meta property="og:title" content="{{ $post->titulo }}">
    <meta property="og:description" content="{!! Tools::cropText($post->texto, 150) !!}">
    <meta property="og:image" content="{{ asset('assets/img/blog/'.$post->imagem) }}">
@else
    <meta property="og:title" content="{{ config('site.title') }}">
    <meta property="og:description" content="{{ config('site.description') }}">
    <meta property="og:image" content="{{ asset('assets/img/'.config('site.share_image')) }}">
@endif
    <meta property="og:site_name" content="{{ config('site.title') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">

    <title>{{ config('site.title') }}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
    @include('frontend.common.header')

    @if(Route::currentRouteName() === 'home')
        @include('frontend.artigos.destaques')
    @endif

    <main class="center">
        <section>
            @yield('content')
        </section>
        <aside>
            @include('frontend.common.aside')
        </aside>
    </main>

    @include('frontend.common.footer')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset("assets/jquery.min.js") }}"><\/script>')</script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
@if(config('site.analytics'))
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{{ config("site.analytics") }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>

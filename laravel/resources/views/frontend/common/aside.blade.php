<div class="busca">
    <form action="{{ route('artigos') }}" method="GET">
        <input type="text" name="busca" placeholder="O que você procura?" required>
        <input type="submit" value="">
    </form>
</div>

@if($contato->facebook || $contato->twitter)
<div class="social">
    @if($contato->facebook)
    <a href="http://facebook.com/{{ $contato->facebook }}" target="_blank" class="facebook">
        <span>{{ $facebookLikes }}</span>
        <span>Likes</span>
    </a>
    @endif

    @if($contato->twitter)
    <a href="http://twitter.com/{{ $contato->twitter }}" target="_blank" class="twitter">
        <span>{{ $twitterFollowers }}</span>
        <span>Seguidores</span>
    </a>
    @endif
</div>
@endif

<div class="categorias">
    <h3>Categorias</h3>
    @foreach($categorias as $c)
    <a href="{{ route('artigos.categoria', $c->slug) }}" @if(isset($categoria) && $c->slug === $categoria->slug) class="active" @endif>
        <span>{{ $c->titulo }}</span>
    </a>
    @endforeach
</div>

@if(Route::currentRouteName() != 'contato' && Route::currentRouteName() != 'perfil' && count($maisComentados))
<div class="mais-comentados">
    <h3>Mais Comentados</h3>
    @foreach($maisComentados as $post)
        <a href="{{ route('artigos.post', [$post->categoriaParent->slug, $post->slug]) }}">
            <img src="{{ asset('assets/img/blog/sidebar/'.$post->imagem) }}">
            {{ $post->titulo }}
        </a>
    @endforeach
</div>
@endif

<div class="newsletter">
    <form action="#" id="form-newsletter">
        <input type="email" name="email" id="newsletter_email" placeholder="E-mail" required>
        <input type="submit" value="Assinar">
    </form>
    <div class="response-wrapper">
        <div id="response-newsletter"></div>
    </div>
</div>

@if(Route::currentRouteName() != 'contato' && Route::currentRouteName() != 'perfil' && $contato->facebook)
<div class="facebook">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4&appId=1485490298412643";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="fb-page" data-href="https://www.facebook.com/{{ $contato->facebook }}" data-width="300" data-height="540" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/{{ $contato->facebook }}"><a href="https://www.facebook.com/{{ $contato->facebook }}"></a></blockquote></div></div>
</div>
@endif
    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ config('site.name') }}</a></h1>
            <nav>
                <a href="{{ route('perfil') }}" @if(str_is('perfil', Route::currentRouteName())) class='active' @endif>Perfil</a>
                <a href="{{ route('artigos') }}" @if(str_is('artigos*', Route::currentRouteName())) class='active' @endif>Artigos</a>
                <a href="{{ route('contato') }}" @if(str_is('contato', Route::currentRouteName())) class='active' @endif>Contato</a>
            </nav>
        </div>
    </header>

    <footer>
        <div class="leia-tambem">
            <div class="center">
                <h2>LEIA TAMBÉM</h2>
                @foreach($leiaTambem as $post)
                <a href="{{ route('artigos.post', [$post->categoriaParent->slug, $post->slug]) }}">
                    {{ $post->titulo }}
                </a>
                @endforeach
            </div>
        </div>

        <div class="copyright">
            <div class="center">
                <img src="{{ asset('assets/img/layout/logo-rodape.png') }}">
                <p>© {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.</p>
                <p><a href="http://trupe.net" target="_blank">Criação de Sites</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a></p>
            </div>
        </div>
    </footer>

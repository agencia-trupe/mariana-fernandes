@extends('frontend.common.template')

@section('content')

    <section class="contato">
        <form action="{{ route('contato.envio') }}" method="post" id="form-contato">
            {{ csrf_field() }}
            <input type="text" name="nome" id="nome" placeholder="Nome" value="{{ old('nome') }}" required>
            <input type="email" name="email" id="email" placeholder="E-mail" value="{{ old('email') }}" required>
            <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required>{{ old('mensagem') }}</textarea>
            <div id="response-contato" @if(count($errors) > 0) class="error" @endif>
                @if(session('success'))
                Mensagem enviada com sucesso!
                @elseif (count($errors) > 0)
                {!! $errors->first() !!}
                @endif
            </div>
            <input type="submit" value="Enviar">
        </form>

        <div class="informacoes">
            <a href="mailto:{{ $contato->email }}?subject=Contato">{{ $contato->email }}</a>
            <p class="telefones">{{ $contato->telefones }}</p>
            <p class="endereco">{{ $contato->endereco }}</p>
        </div>

        <div class="googlemaps">
            {!! $contato->googlemaps !!}
        </div>
    </section>

@endsection
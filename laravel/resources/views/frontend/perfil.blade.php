@extends('frontend.common.template')

@section('content')

    <article class="perfil">
        <div class="texto">{!! $perfil->texto !!}</div>
        <div class="imagem">
            <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}">
        </div>
    </article>

@endsection
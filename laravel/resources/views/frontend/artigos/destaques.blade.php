@if(count($destaques))
<div class="destaques center">
    @foreach($destaques as $post)
    <a href="{{ route('artigos.post', [$post->categoriaParent->slug, $post->slug]) }}">
        <img src="{{ asset('assets/img/blog/destaque/'.$post->imagem) }}">
        <div class="overlay">
            <div class="titulo">
                <h4>{{ $post->categoriaParent->titulo }}</h4>
                <h3>{{ $post->titulo }}</h3>
            </div>
            <span>Leia Mais</span>
        </div>
    </a>
    @endforeach
</div>
@endif
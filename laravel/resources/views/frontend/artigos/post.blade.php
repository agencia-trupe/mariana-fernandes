@extends('frontend.common.template')

@section('content')

    <section class="artigos-post">
        <div class="post">
            <img src="{{ asset('assets/img/blog/'.$post->imagem) }}">
            <h2>{{ $post->titulo }}</h2>
            <div class="informacoes">
                <span class="data">{{ Tools::formataData($post->data) }}</span>

                <a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-text="" data-hashtags="">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
            </div>
            <div class="texto">{!! $post->texto !!}</div>
        </div>

        @if(count($post->comentariosAprovados))
        <div class="comentarios">
            <h3>Comentários</h3>
            @foreach($post->comentariosAprovados as $comentario)
            <div class="comentario">
                <p class="autor">{{ $comentario->autor }} <span>em {{ $comentario->created_at }}</span></p>
                <p class="texto">{{ $comentario->comentario }}</p>
            </div>
            @endforeach
        </div>
        @endif

        <form action="#" id="form-comentario" data-url="{{ Request::url() }}">
            <h3>Deixe seu comentário</h3>
            <input type="text" name="autor" id="autor" placeholder="Nome" required>
            <input type="email" name="email" id="comentario_email" placeholder="E-mail" required>
            <textarea name="comentario" id="comentario" placeholder="Comentário" required></textarea>
            <input type="submit" value="Comentar">
            <div id="response-comentario"></div>
        </form>
    </section>

@endsection
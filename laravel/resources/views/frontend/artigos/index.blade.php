@extends('frontend.common.template')

@section('content')

    <section class="artigos-lista">
        @if(count($posts) < 1 && isset($busca))
             <h1 class="not-found">
                <span>Sua busca não retornou nenhum resultado para <strong>{{ $busca }}</strong>.<br>Tente buscar por outros termos.</span>
            </h1>
        @elseif(!count($posts))
            <h1 class="not-found">
                <span>Nenhum post encontrado.</span>
            </h1>
        @else
            @foreach($posts as $post)
            <a href="{{ route('artigos.post', [$post->categoriaParent->slug, $post->slug]) }}" class="post">
                <img src="{{ asset('assets/img/blog/'.$post->imagem) }}">
                <h2>{{ $post->titulo }}</h2>
                <p>{!! Tools::cropText($post->texto, 250) !!}</p>
                <span class="leia-mais">Leia mais</span>
            </a>
            @endforeach
        @endif

        <div class="paginacao">
            @if(isset($busca))
            {!! $posts->appends(['busca' => $busca])->render(new \App\Pagination\SimplePresenter($posts)) !!}
            @else
            {!! $posts->render(new \App\Pagination\SimplePresenter($posts)) !!}
            @endif
        </div>
    </section>

@endsection
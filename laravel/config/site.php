<?php

return [

    'name'        => 'Mariana Fernandes',
    'title'       => 'Mariana Fernandes',
    'description' => 'O blog é um voltado para jovens advogados e interessados em direito em geral, com dicas para advogados, com organização de escritório, bem como assuntos do dia a dia do mundo jurídico.',
    'keywords'    => 'advocacia, direito, estudantes de advocacia, jovens advogados, artigos sobre direito, escritório jurídico, direito, direito em geral, mundo jurídico',
    'share_image' => 'layout/logo-mariana.png',
    'analytics'   => 'UA-68531510-1'

];

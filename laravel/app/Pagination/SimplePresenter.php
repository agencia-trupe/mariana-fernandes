<?php

namespace App\Pagination;

use Illuminate\Pagination\SimpleBootstrapThreePresenter;

class SimplePresenter extends SimpleBootstrapThreePresenter
{
    public function render()
    {
        if ($this->hasPages()) {
            return sprintf(
                '<ul class="pager">%s %s</ul>',
                $this->getPreviousButton('&laquo; Anteriores'),
                $this->getNextButton('Próximos &raquo;')
            );
        }

        return '';
    }
}
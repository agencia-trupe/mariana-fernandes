<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('blog', 'App\Models\BlogPost');
        $router->model('destaques', 'App\Models\BlogPost');
        $router->model('categorias', 'App\Models\BlogCategoria');
        $router->model('comentarios', 'App\Models\BlogComentario');
        $router->model('perfil', 'App\Models\Perfil');
        $router->model('contato', 'App\Models\Contato');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('categoria_slug', function($value) {
            return \App\Models\BlogCategoria::slug($value)->first() ?: \App::abort('404');
        });

        $router->bind('post_slug', function($value) {
            return \App\Models\BlogPost::with('categoriaParent', 'comentarios')->slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}

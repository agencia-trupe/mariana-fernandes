<?php

namespace App\Providers;

use App\Helpers\Tools;
use App\Models\Contato;
use App\Models\BlogPost;
use App\Models\BlogCategoria;
use App\Models\ContatoRecebido;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', ContatoRecebido::naoLidos()->count());
        });

        view()->composer('frontend.common.template', function($view) {
            $contato = Contato::first();
            $cacheTime = 60;

            $facebookLikes = Cache::remember('facebookLikes', $cacheTime, function() use ($contato) {
                return Tools::getFacebookLikes($contato->facebook);
            });
            $twitterFollowers = Cache::remember('twitterFollowers', $cacheTime, function() use ($contato) {
                return Tools::getTwitterFollowers($contato->twitter);
            });

            $maisComentados = BlogPost::with('categoriaParent')->maisComentados()->take(3)->get();
            $leiaTambem = BlogPost::with('categoriaParent')->random()->take(4)->get();

            $view->with('categorias', BlogCategoria::ordenados()->get());
            $view->with('contato', Contato::first());
            $view->with(compact('facebookLikes', 'twitterFollowers', 'leiaTambem', 'maisComentados'));
        });

        view()->composer('frontend.artigos.destaques', function($view) {
            $view->with('destaques', BlogPost::with('categoriaParent')->destaque()->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

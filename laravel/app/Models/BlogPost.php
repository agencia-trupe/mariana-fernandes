<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'blog_posts';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('blog_categoria_id', $categoria_id);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', "%$termo%")->orWhere('texto', 'LIKE', "%$termo%");
    }

    public function scopeDestaque($query)
    {
        return $query->where('destaque_home', 1);
    }

    public function scopeRandom($query)
    {
        return $query->orderByRaw('RAND()');
    }

    public function scopeMaisComentados($query)
    {
        return $query->join('blog_comentarios', function($join) {
                $join->on('blog_comentarios.blog_post_id', '=', 'blog_posts.id')
                     ->where('blog_comentarios.aprovado', '=', 1);
            })
            ->groupBy('blog_posts.id')
            ->orderBy(\DB::raw('count(blog_comentarios.id)'), 'DESC');
    }

    public function categoriaParent()
    {
        return $this->belongsTo('App\Models\BlogCategoria', 'blog_categoria_id');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\BlogCategoria', 'blog_categoria_id');
    }

    public function comentarios()
    {
        return $this->hasMany('App\Models\BlogComentario', 'blog_post_id')->orderBy('created_at', 'DESC');
    }

    public function comentariosAprovados()
    {
        return $this->hasMany('App\Models\BlogComentario', 'blog_post_id')->orderBy('created_at', 'DESC')->where('aprovado', 1);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogComentario extends Model
{
    protected $table = 'blog_comentarios';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('id', 'DESC');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\BlogPost', 'blog_post_id');
    }
}

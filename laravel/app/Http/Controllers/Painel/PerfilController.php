<?php

namespace App\Http\Controllers\Painel;

use App\Helpers\CropImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\PerfilRequest;
use App\Models\Perfil;
use Illuminate\Http\Request;

class PerfilController extends Controller
{
    private $image_config = [
        'width'  => 290,
        'height' => null,
        'path'   => 'assets/img/perfil/'
    ];

    public function index()
    {
        $perfil = Perfil::first();

        return view('painel.perfil.index', compact('perfil'));
    }

    public function update(PerfilRequest $request, Perfil $perfil)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $perfil->update($input);
            return redirect()->route('painel.perfil.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}

<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogDestaquesRequest;
use App\Models\BlogCategoria;
use App\Models\BlogPost;
use Illuminate\Http\Request;

class BlogDestaquesController extends Controller
{
    private $limite = 4;

    public function index()
    {
        $destaques = BlogPost::ordenados()->destaque()->get();
        $limite    = $this->limite;

        return view('painel.blog.destaques.index', compact('destaques', 'limite'));
    }

    public function create()
    {
        $categorias = BlogCategoria::ordenados()->lists('titulo', 'id');

        return view('painel.blog.destaques.create', compact('categorias'));
    }

    public function store(BlogDestaquesRequest $request)
    {
        if (count(BlogPost::destaque()->get()) >= $this->limite) {
            return redirect()->route('painel.blog.destaques.index')->withErrors(['Número máximo de destaques atingido.']);
        }

        $post = BlogPost::find($request->get('blog_post_id'));
        if (! $post) abort('404');

        try {

            $post->destaque_home = 1;
            $post->save();

            return redirect()->route('painel.blog.destaques.index')->with('success', 'Destaque adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar destaque: '.$e->getMessage()]);

        }
    }

    public function destroy(BlogPost $post)
    {
        try {

            $post->destaque_home = 0;
            $post->save();
            return redirect()->route('painel.blog.destaques.index')->with('success', 'Destaque excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir destaque: '.$e->getMessage()]);

        }
    }
}

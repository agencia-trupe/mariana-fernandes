<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Perfil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil = Perfil::first();

        return view('frontend.perfil', compact('perfil'));
    }
}

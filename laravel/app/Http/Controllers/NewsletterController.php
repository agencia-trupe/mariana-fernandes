<?php

namespace App\Http\Controllers;

use App\Models\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsletterRequest;

class NewsletterController extends Controller
{
    public function index(NewsletterRequest $request)
    {
        Newsletter::create($request->all());

        $response = [
            'status'  => 'success',
            'message' => 'Cadastro efetuado com sucesso!'
        ];

        return response()->json($response);
    }
}

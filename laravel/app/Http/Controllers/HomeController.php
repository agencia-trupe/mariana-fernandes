<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\BlogPost;
use Illuminate\Http\Request;
use App\Models\BlogCategoria;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogComentariosRequest;

class HomeController extends Controller
{
    public function index(BlogCategoria $categoria, Request $request)
    {
        $posts = BlogPost::with('categoriaParent')->ordenados();
        $busca = $request->get('busca');

        if (!empty($categoria->id)) $posts = $posts->categoria($categoria->id);

        if ($busca) $posts = $posts->busca($busca);

        $posts = $posts->simplePaginate(5);

        view()->share(compact('categoria'));

        return view('frontend.artigos.index', compact('posts', 'busca'));
    }

    public function show(BlogCategoria $categoria, BlogPost $post)
    {
        view()->share(compact('post'));
        return view('frontend.artigos.post');
    }

    public function comentario(BlogCategoria $categoria, BlogPost $post, BlogComentariosRequest $request)
    {
        $post->comentarios()->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => 'Comentário enviado com sucesso! Sujeito a aprovação da moderacão.'
        ];

        return response()->json($response);
    }
}

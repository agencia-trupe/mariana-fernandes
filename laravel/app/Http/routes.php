<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('artigos', ['as' => 'artigos', 'uses' => 'HomeController@index']);
Route::get('artigos/{categoria_slug}', ['as' => 'artigos.categoria', 'uses' => 'HomeController@index']);
Route::get('artigos/{categoria_slug}/{post_slug}', ['as' => 'artigos.post', 'uses' => 'HomeController@show']);
Route::post('artigos/{categoria_slug}/{post_slug}', ['as' => 'artigos.comentario', 'uses' => 'HomeController@comentario']);
Route::get('perfil', ['as' => 'perfil', 'uses' => 'PerfilController@index']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);
Route::post('newsletter', ['as' => 'newsletter', 'uses' => 'NewsletterController@index']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);

    Route::get('blog/comentarios/{comentarios}/aprovacao/{value}', 'BlogComentariosController@aprovacao');
    Route::get('blog/posts/{categorias}', 'BlogPostsController@posts');
    Route::resource('blog/categorias', 'BlogCategoriasController');
    Route::resource('blog/comentarios', 'BlogComentariosController');
    Route::resource('blog/destaques', 'BlogDestaquesController');
    Route::resource('blog', 'BlogPostsController');
    Route::resource('perfil', 'PerfilController');
    Route::resource('usuarios', 'UsuariosController');
    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');
    Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
    Route::resource('newsletter', 'NewsletterController');

    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});

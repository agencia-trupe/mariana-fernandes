<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required|unique:newsletter,email'
        ];
    }

    public function messages()
    {
        return [
            'email.email'    => 'Insira um endereço de e-mail válido',
            'email.required' => 'Insira um endereço de e-mail válido',
            'email.unique'   => 'O e-mail inserido já está cadastrado',
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogPostsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'blog_categoria_id' => 'required',
            'data'              => 'required',
            'titulo'            => 'required',
            'imagem'            => 'required|image',
            'texto'             => 'required'
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}

<?php

namespace App\Helpers;

use Abraham\TwitterOAuth\TwitterOAuth;

class Tools
{

    public static function getFacebookLikes($user)
    {
        $appId = '1485490298412643';
        $appSecret = '253f0f31b9c6a588160715c1b2c9bdd6';
        $url = 'https://graph.facebook.com/v2.4/'. $user .'/?access_token='. $appId . '|' . $appSecret . '&fields=likes';

        $json = @file_get_contents($url);

        return $json ? json_decode($json)->likes : 0;
    }

    public static function getTwitterFollowers($user)
    {
        $consumerkey = "ZwvruYhkZvLqDPzxNfpojxsih";
        $consumersecret = "urHIahB6eXhfhhIK0y0PyMu2QKZQHRoLQxs160Pc8xkWvL7tI8";
        $accesstoken = "41056456-oUqmHjRGmlSHlGFCca301MRWnq2gEQcgYUfyljZ6m";
        $accesstokensecret = "y67ucXOs4uOrHfW2DdE9b1cdIWJfYzMtjsKgkZmRZQJRk";

        $connection = new TwitterOAuth($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);
        $user = $connection->get("users/show", array("screen_name" => $user));

        return $connection->getLastHttpCode() == 200 ? $user->followers_count : 0;
    }

    public static function cropText($text = null, $length = null)
    {
        if (!$text || !$length) return false;

        $length = abs((int)$length);

        if(strlen($text) > $length) {
            $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', strip_tags($text));
        }

        return $text;
    }

    public static function formataData($data = null)
    {
        $meses = array(
            '01' => 'JAN',
            '02' => 'FEV',
            '03' => 'MAR',
            '04' => 'ABR',
            '05' => 'MAI',
            '06' => 'JUN',
            '07' => 'JUL',
            '08' => 'AGO',
            '09' => 'SET',
            '10' => 'OUT',
            '11' => 'NOV',
            '12' => 'DEZ'
        );

        if ($data) {
            list($dia, $mes, $ano) = explode('/', $data);
            return $dia.'/'.$meses[$mes].'/'.$ano;
        }
    }

}

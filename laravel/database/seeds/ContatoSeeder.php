<?php

use Illuminate\Database\Seeder;

class ContatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'telefones'  => '11 3106-8741 | 3106-8758',
            'email'      => 'mariana@marianafernandes.com.br',
            'endereco'   => 'Rua Maria Paula, 62 - conj. 41 - Bela Vista, São Paulo - SP',
            'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.451689038027!2d-46.638277599999995!3d-23.552215699999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59ac4347ee13%3A0xa54ac477d24b8c8e!2sCondom%C3%ADnio+Edif%C3%ADcio+Cavaru+-+R.+Dona+Maria+Paula%2C+62+-+Bela+Vista%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1442840923713" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'facebook'   => 'facebook',
            'twitter'    => 'twitter',
        ]);
    }
}
